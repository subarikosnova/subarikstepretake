// ===============================================
$(document).ready(function () {
    let preservedScroll = {};

    function updateBackgroundHeight() {
        const totalImageHeight = $('.amazing-word__img-list:visible').outerHeight();
        $(".our-amazing-background").css("height", totalImageHeight + "px");
    }

    $('.amazing-word__img-list').not(':first').hide();
    updateBackgroundHeight();

    $('.our-amazing-work__nav-text').click(function () {
        const tabId = $(this).data('img');
        $('.our-amazing-work__nav-text').removeClass('active2');
        $(this).addClass('active2');

        $('.amazing-word__img-list').hide();
        $('#' + tabId).show();

        updateBackgroundHeight();

        if (preservedScroll[tabId]) {
            $(".our-amazing-background").css("overflow", "auto");
        } else {
            $(".our-amazing-background").css("overflow", "hidden");
        }

        $('.btn-amazing-container').hide();
        $('#loadMoreButton' + tabId.substr(3)).show();
    });

    $('.load-more-button').click(function () {
        const activeContainer = $(this).closest('.amazing-word__img-list');
        const hiddenImagesContainer = $('#imgContainer' + activeContainer.attr('id').substr(3));
        const hiddenImages = hiddenImagesContainer.find('img:hidden');

        // Додайте клас "loading" для кнопки "Load More"
        $(this).addClass('loading');

        // Чекаємо 2 секунди, після чого додайте картинки на сторінку
        setTimeout(function () {
            for (let i = 0; i < 12 && i < hiddenImages.length; i++) {
                activeContainer.append(hiddenImages[i]);
            }

            if (hiddenImages.length <= 12) {
                $(this).hide();
            }

            const tabId = activeContainer.attr('id');
            $(".our-amazing-background").css("overflow", "auto");
            updateBackgroundHeight();

            preservedScroll[tabId] = true;

            $(this).parent('.btn-amazing-container').css("top", "97%");
            $(this).parent('.btn-amazing-container').css("padding-bottom", "24px");

            $('.load-more-button').removeClass('loading');
        }.bind(this), 2000);
    });
});
