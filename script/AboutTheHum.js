$(document).ready(function () {
    $(".sliders").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        // autoplay: true,
        // autoplaySpeed: 2000,
        centerMode: true,
        prevArrow: '<button type="button" class="btn slider-btn slider-prev"><img src="images/aboutthehum/arrow-left-solid.svg" alt="arrow-left"></button>',
        nextArrow: '<button type="button" class="btn slider-btn slider-next"><img src="images/aboutthehum/arrow-right-solid.svg" alt="arrow-right"></button>',
        asNavFor: '.sliders-two',
        responsive: [{
            breakpoint: 1051,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }],
        onAfterChange: function (slick, currentSlide) {
            $(".slider-text").hide();
            $(".slick-active .slider-text").show();
        }
    });

    $(".sliders-two").slick({
        infinite: true,
        centerMode: true,
        // centerPadding: '10px',
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.sliders',
        dots: false,
        arrows: false,
        focusOnSelect: true,
        // autoplay: true,
        // autoplaySpeed: 2000,
        responsive: [
            // {
            //     breakpoint: 69,
            //     settings: {
            //         arrows: false,
            //         centerMode: true,
            //         centerPadding: '30px',
            //         slidesToShow: 3
            //     }
            // },
            // {
            //     breakpoint: 451,
            //     settings: {
            //         arrows: false,
            //         centerMode: true,
            //         centerPadding: '40px',
            //         slidesToShow: 2
            //     }
            // }
        ]
    });
});
